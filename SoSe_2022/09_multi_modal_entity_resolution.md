# Multi-modal Entity Resolution for Product Matching

## Motivation

Entity Resolution has been applied successfully to match product offers from different web shops. Unfortunately, in certain domains the (textual or numerical) attributes of a product are not sufficient for a reliable match decision. The use of images is a promising strategy to improve product matching results in the fashion domain.
Over the last years, convolutional neural networks (CNN) have achieved many breakthroughs in the field of computer vision and have contributed much to the wide adoption of deep learning. An adaption of deep learning for fashion images called Match-R-CNN is presented in [DeepFashion2](https://openaccess.thecvf.com/content_CVPR_2019/papers/Ge_DeepFashion2_A_Versatile_Benchmark_for_Detection_Pose_Estimation_Segmentation_and_CVPR_2019_paper.pdf). It supports the identification and classification of fashion items as well as image retrieval. For attribute-based ER, deep learning approaches internally work mostly with the concept of distributed representations (embeddings). The combination of text and image data in deep learning systems is called multimodal deep learning. Typical applications include the creation of text description for images or image retrieval fromtext queries. An overview on tasks, datasets and problems in this field can be found in [survey1](https://dl.acm.org/doi/pdf/10.1613/jair.1.11688).


## Goal
Your task in this practicum is to implement an approach for multi-modal entity resolution for product matching. This will also include evaluating the implemented approach on selected benchmark datasets.


## Work packages

### 1 Concept 

Make yourself familiar with the general concepts of multi-modal deep learning and entity resolution.
Create an initial sketch for the architecture of your solution. 

### 2 Implementation

Implement your application. Your code needs to be documented and tested to ensure future usability and correctness. At the end of this phase you should also run your system on benchmark datasets to see how well the implemented approach perfoms.

### 3 Presentation

The presentation (10 min + 5 min discussion) should showcase how your system works (architecture and basic ideas), as well as present the results. 

### Literature & Links

- [DeepFashion2](https://openaccess.thecvf.com/content_CVPR_2019/papers/Ge_DeepFashion2_A_Versatile_Benchmark_for_Detection_Pose_Estimation_Segmentation_and_CVPR_2019_paper.pdf)
- [MAPS](https://openaccess.thecvf.com/content/WACV2022/papers/Das_MAPS_Multimodal_Attention_for_Product_Similarity_WACV_2022_paper.pdf)
- [WR](https://dbs.uni-leipzig.de/file/GvDB21_multi_modal_product_matching.pdf)
- [survey1](https://dl.acm.org/doi/pdf/10.1613/jair.1.11688)
- [survey2](https://arxiv.org/pdf/2105.11087.pdf)


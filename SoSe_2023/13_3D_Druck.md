# Klassifizierung von qualitätsmindernden Eigenschaften im 3D-Druck
Das selektive Laserschmelzverfahren wird in der Industrie unter anderem zur Herstellung von Metallbauteilen verwendet und nimmt einen immer größeren Stellenwert in der Produktionsindustrie ein. Es ist ein additives Fertigungsverfahren, bei dem ein Metallpulverbett gleichmäßig in Schichten auf eine Bauteilplattform aufgebracht und mithilfe eines Lasers aufgeschmolzen wird. Auf diese Weise ist es möglich, Bauteile mit vergleichsweise individuell hohen geometrischen Ansprüchen zu fertigen. Dabei kommt es zu wiederholten Aufheiz- und Abkühlzyklen, bei denen die Bauteile dauerhaft hohen Belastungen ausgesetzt sind. Um einen hohen Qualitätsstandard zu gewährleisten, sollten fehlerhaft gedruckte Schichten vermieden werden. Defekte Schichten weisen beispielsweise Poren, Risse oder Unebenheiten auf, die zu qualitätsmindernden Eigenschaften führen. Erst nach dem Druck werden die Bauteile respektive Testbauteile auf Zug- und Druckfestigkeit geprüft, um die Bauteilqualität sicherzustellen. Bei negativen Testergebnissen wird das Bauteil analysiert und erneut mit optimierten Prozessparametern gedruckt, was den Prozess kostenintensiv und zeitaufwändig macht. 
Mithilfe von maschinellen Lernverfahren ist es möglich, fehlerhafte Schichten zu klassifizieren und 3D Druckprozesse zu optimieren. 

## Zielstellung:
Zu diesem Zweck soll ein Klassifikationsmodell auf der Grundlage von Graustufenbildern aus realen Druckexperimenten entwickelt werden. Diese Graustufenbilder enthalten sowohl qualitativ hochwertige Schichten als auch Schichten mit qualitätsmindernden Eigenschaften. 

## 1.	Datenvorverarbeitung: 
Die Datensätze müssen im Voraus beschriftet und mit bekannten Vorverarbeitungsschritten für ein geeignetes Training von Deep Learning-Methoden vorbereitet werden.
## 2. Modellierung:
Anschließend ist ein für das Problem geeignetes maschinelles Lernmodell auszuwählen und eine geeignete ML-Bibliothek zu finden, welches auf der Grundlage der Daten trainiert wird. Die Klassifizierungsgenauigkeit ist mithilfe von Metriken wie Precision, Recall und F1 Score zu evaluieren. 
## 3.Vortrag:
Ausarbeitung einer 10-minütigen Präsentation, in welcher die Aufgabenstellung, die Lösungsskizze (Modell- und Merkmalsauswahl) und die Ergebnisse dargestellt werden


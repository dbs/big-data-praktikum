# Analyse und Vorhersage städtischer Emissionen

## Motivation
Im Jahr 2019 veröffentlichten Nangini et al. [1] einen globalen Datsensatz, welcher die CO2-Emissionen von 343 Städten mit Meta-Informationen zu Sozioökonomie, Verkehr und Klima zusammenführt.
Aus diesen Daten lassen sich möglicherweise relevante Zusammenhänge erschließen, welche helfen können den Fokus auf die entscheidenen Emissions-Faktoren im städtischen Raum zu legen.
Außerdem kann eine solche Analyse dazu Beitragen, den Einfluss von fixen Faktoren wie Population oder Lage/Klimazone einer Stadt zu klären.

<details><summary>Übersicht der Zusammengeführten Emissions- und Metadaten</summary>
<img src="images/03_city_emissions.webp" alt= “Übersicht der Zusammengeführten Emissions- und Metadaten” width="100%" height="75%">
</details>

## Zielstellung
Ziel des Praktikums ist es, anhand einer Datenanalyse die treibenden Faktoren für Emissionsunterschiede (aller Art) zwischen den gegebenen Städten herauszustellen.
Anschließend soll mittels eines grundlegenden Machine Learning Ansatzes (z.B. logistische Regression) die Entwicklung der Emissionen vorausgesagt werden, um nach Möglichkeit die ortsabhängige Dringlichkeit einschränkender Maßnahmen zu bestimmen.

## Arbeitspakete

### 1. Einarbeitung und Lösungsskizze
Zunächst soll in einem Konzept skizziert werden, mit welchen statistischen Methoden, Analyse-Tools und Machine Learning Verfahren vorgegangen werden soll.

### 2. Implementierung und Evaluation
Anhand dieser Lösungsskizze sind Analyse und experimenteller Aufbau umzusetzen und mit passenden Mitteln zu evaluieren (Grafiken, relevante Vergleiche zu anderen Statistiken).


### 3. Vortrag
Ausarbeitung einer 10-minütigen Präsentation, in welcher die Aufgabenstellung, die Lösungsskizze und die Ergebnisse dargestellt werden.

## Links

* [1] [Paper zum Datensatz](https://www.nature.com/articles/sdata2018280)
* [Link zum Datensatz](https://doi.pangaea.de/10.1594/PANGAEA.884141)
* [Kartenübersicht der Daten](http://www.globalcarbonatlas.org/global-carbon-cities)

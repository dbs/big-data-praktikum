# Training Data Generation for Privacy Preserving Record Linkage using Feaderated Learning

## Motivation
Privacy Preserving Record Linkage (PPRL) befasst sich mit der Integration von sensitiven Daten, wie z. B. Patientendaten. 
Das Ziel dieser Verfahren ist die Identifikation von Datensätzen, welche dieselbe Realweltentität repräsentieren und von verschiedenen Parteien, 
wie z. B. Krankenhäuser, Krankenkassen oder Gesundheitsämter, stammen. Personenbezogene Daten sind sensitiv, dass heißt, die Daten müssen geschützt werden 
und dürfen nicht in Klartext an andere Parteien übermittelt werden. Um dennoch eine Integration zu ermöglichen, 
werden ausgewählte Attribute der Datensätze codiert (maskiert), so dass der Attributwert nicht mehr erkennbar ist. 
Die drei wesentlichen Herausfoderungen im Rahmen von PPRL sind Skalierbarkeit auf großen Datenmenge mit mehreren Millionen Datensätzen, 
dass Erreichen einer hohen Linkage-Qualität sowie die Einhaltung der Privatsphäre. 


| ![bloom filter](images/pprl_bloom_filter.PNG "Bloom Filter") | 
|:--:|
| Beispiel für die Erstellung eines Bloom Filters mittel N-Grammen und 2 Hashfunktionen. Jede Hash-Funktion bildet auf eine Bit-Position eines Bit-Vektors ab. |

Die Bloom-Filter werden von jedem Datenbesitzer generiert und zu einer Linkage-Unit gesendet, welche die Berechnung der Duplikate durchführt. Um Duplikate zu bestimmen wird
für jedes Bloom-Filter Paar Ähnlichkeiten bestimmt, wie z.B. Jaccard-Similarity, bei der das Verhältnis der gemeinam gesetzten Bits und die Gesamtanzahl aller gesetzten Bits gebildet wird.

Die Mehrheit der Lösung verwendet eine Threshold-basierte Klassifikation, bei der vorab eine Mindestähnlichkeit definiert wird und alle Paare, die eine höhere Ähnlichkeit erreichen als Match klassifiziert werden. Ein Nachteil dieser Methode ist, das beide Klassen Match und Non-Match nur durch einen Wert trennbar sein müssen, was nicht immer der Fall sein muss. Weiterhin ist die Threshold-Bestimmung im Privacy-Kontext nicht trivial.

Neue Ansätze [1] verwenden Federated Learning-Ansätze bei denen lokale Modelle trainiert werden und diese Modelle global aggregiert werden, um die Klassifikation durchzuführen. Jedoch lässt der Ansatz [1] offen, wie die Trainingsdaten konkret in der Praxis generiert werden können.



## Zielstellung
Es soll ein Ansatz evaluiert werden, bei dem lokale Traningsdaten für jeden Datenbesitzer mithilfe eines öffentlichen Datensatzes generiert werden. Die lokalen Trainingsdaten sollen für die Erstellung eines lokalen Modells verwendet werden. Die erstellten Modelle werden durch die Linkage-Unit zu einem globalen Modell aggregiert. Das aggregierte Modell wird für die Klassifikation der BF-Paare verwendet. Dabei klassifiziert das Modell ein Ähnlichkeitsvektor, welches ein Bloom-Filter Paar darstellt basierend auf dem Vergleich der Bloom-Filter mittels verschiedener Ähnlichkeitsfunktionen. 


## Arbeitspakete

1. <b> Trainingsdatengenerierung</b><br>
  Im ersten Schritt müssen die BF der Datenbesitzer mit den öffentlichen Daten verglichen und gelabelt werden. Die Annahme ist, dass die Klasse Match oder Non-Match bereits bekannt ist. Um die Privatsphäre der ursprünglichen Daten zu erhöhen, sollen die Daten für das Training als auch für das spätere Linkage verrauscht werden. Hierfür soll das Verfahren BLIP aus [1] genutzt werden.
    
2. <b> Training</b><br>
   Mittels der lokalen Trainingsdaten können die lokalen Modelle generiert werden und daraus resultierend das Modell für die Linkage-Unit. 

3.  <b>Evaluation</b><br>
    Alle Konfigurationen sollen bzgl. des resultierenden Matchergebnis evaluiert werden mittels Precision, Recall und F-Measure. 
4. <b>Vortrag</b><br>
   Ausarbeitung einer 10-minütigen Präsentation, in welcher die Aufgabenstellung, die Lösungsskizze (kurze Beschreibung des Algorithmus, 
   Nennung der verwendeten Bibliotheken) und die Ergebnisse der Evaluierung dargestellt werden.

## Datensätze
* NorthCarolina Voter Datensatz 

## Literatur
[1] Ranbaduge, Thilina, Dinusha Vatsalan, and Ming Ding. "Privacy-preserving Deep Learning based Record Linkage." arXiv preprint arXiv:2211.02161 (2022).

# Effiziente Ableitung von Teildatensätzen aus MongoDB

## Motivation
Ein wichtiger Bestandteil von Datenintegrationprozessen mit mehreren Datenquellen ist die Erkennung von Datensätzen, die zur gleichen Real-Welt-Entität, z.B. einer Person, gehören. Bei diesem Record Linkage werden Datensätze i.d.R. paarweise anhand ihrer Attribute wie Name, Geburtsdatum und Adresse verglichen und als Match bzw. Non-Match klassifiziert.
Für Training und Validierung dieses Klassifiziers werden Testdaten benötigt.

Eine Quelle ist die North Carolina Voter Registry, welche reale Personendaten in Snapshots enthält, sodass Veränderungen der Attribute (z.B. durch Umzüge oder Namensänderungen) abgebildet werden.
Die Original-CSV-Daten wurden durch [Panse2021] aufbereitet und in eine MongoDB überführt.
Die Datenbank hat eine Größe von ~330GB (unkomprimiert) bzw. 74GB (MongoDB storage size).

## Zielstellung
Ziel des Praktikums ist es, aus dieser Datenbank verschiedene Teildatensätze als Testdaten für Record Linkage zu extrahieren.
Dabei sollen folgende Parameter konfiguierbar sein:

- Anzahl von Quellen (bzw. Snapshots)
- Datensatzgröße
- Überlappung
- zeitlicher Abstand der verwendeten Snapshots
- Fehlerhäufigkeit
- Seed für zufällige Auswahl entsprechend dieser Kriterien

## Arbeitspakete

### 1. Einarbeitung und Lösungsskizze
Zunächst soll der Datensatz in eine MongoDB Instanz importiert und analysiert werden.
Welche Informationen zu den Records und Duplikaten sind enthalten und wie sind sie strukturiert?

Anschließend soll in einem Konzept skizziert werden, mit welchen Queries die o.g. Datensatzvarianten abgefragt werden können. Um die Queries möglichst effizient auszuführen, sollten die Original-Daten zunächst in ein optimiertes Schema überführt werden.

### 2. Implementierung und Evaluierung
Basierend auf der Lösungsskizze ist die Extraktion von Teildatensätzen zu implementieren.
Dabei sollen die benötigten Laufzeiten gemessen werden.
Die erzeugten Datensätze sind auf die Erfüllung der vorgegebenen Parameter zu prüfen.

### 3. Vortrag
Ausarbeitung einer 10-minütigen Präsentation, in welcher die Aufgabenstellung, die Lösungsskizze und die Ergebnisse dargestellt werden.

## Material

* [NCVR Historical Voter Registration Snapshots](https://www.ncsbe.gov/results-data/voter-registration-data#historical-data)
* [Panse et al., 2021: Generating Realistic Test Datasets for Duplicate Detection at Scale Using Historical Voter Data](https://openproceedings.org/2021/conf/edbt/p152.pdf)
* Der aufbereitete Datensatz von [Panse2021] wird zu Beginn des Praktikums bereitstellt.
* [MongoDB Community Edition](https://www.mongodb.com/try/download/community)
* [MongoDB Compass](https://www.mongodb.com/docs/compass/master/)
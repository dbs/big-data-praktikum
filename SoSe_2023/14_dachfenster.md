# Erkennen von Dachfenstern in Befliegunsdaten

**Voraussetzung**: Python-Programmierkenntnisse

## Motivation

Um Modernisierungs- und Baumaßnahmen geziehlt zu fördern wollen Kommunen wissen wie breitflächig Dachgeschosse ausgebaut sind. Leider kann man nicht mit Sicherheit davon ausgehen, dass diese Informationen in allen Bauämtern in allen Kommunen (digital) vorliegen. Daher soll versucht werden in Satelieten- /Luftbildern zu erkennen ob in einem Dach große Schrägfenster verbaut sind, was auf eine Dachgeschosswohnung hindeutet. 

## Zielstellung

Im Projekt soll mit Python-Frameworks wie z.B. Detectron2 und YOLO vortrainiertes Modelle feingetuned und verglichen werden. Es ist ein Datensatz vorhanden welcher aus Bildern von je einem Hausdach pro Bild besteht. Die Bilder müssen jedoch noch selbst gelabelt werden. Danach gilt es ein geeignetes Modell zu wählen und passende Hyperparameter zu wählen um ein gutes Ergebniss zu erziehlen. 

## Arbeitspakete

### 1. Lösungsskizze erarbeiten und Daten vorbereiten
Zunächst müssen mit einem Labeling-Tool wie Label-Studio Dachfenster in einigen Bildern markiert werden. Anschließend soll in einem Konzept skizziert werden, welche Frameworks zur verfügung stehen um das Modell zu trainieren. 

### 2. Implementierung und Evaluierung
Basierend auf der Lösungsskizze sollen die gelabelten Daten in Trainings und Testdaten geteilt werden und mehrere Modelle trainiert werden. Die trainierten Modelle sollen dann mithilfe des Testdatensatzen und geeigneten Metriken miteinander verglichen werden. 

### 3. Vortrag
Ausarbeitung einer 10-minütigen Präsentation, in welcher die Aufgabenstellung, die Lösungsskizze und die Ergebnisse dargestellt werden.
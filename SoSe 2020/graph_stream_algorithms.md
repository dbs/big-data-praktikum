# Graph-Stream-Algorithmen

## Motivation
Streamanalyse ist derzeit ein populäres Gebiet der Forschung. Graph-Streams und die dazugehörigen 
Graph-Stream-Algorithmen erfahren dagegen noch wenig Aufmerksamkeit. Mit der folgenden Aufgabe soll ein 
Einstieg in den Bereich der Algorithmen auf Graph-Streams erfolgen. Besonders der Unterschied der 
Implementierungen zwischen Insert-Only-, Windowed- sowie Graph-Sketch-Streams ist von Interesse.

## Zielstellung
Ziel ist die Implementierung von Graph-Stream-Algorithmen die eine Auswahl von Graph-Statistiken 
für einen beliebigen Graph-Stream erzeugen können.

## Arbeitspakete
### 1. Auswahl relevanter Algorithmen
Die Auswahl der Algorithmen erfolgt in Zusammenarbeit mit dem Betreuer. Es sollte darauf geachtet werden, 
dass möglichst Algorithmen aus verschiedenen Bereichen der Graphanalyse ausgewählt werden. Z.B. `Degree`, 
`Score`, `Density`, `Clustering`, `Communities`, `Distributions` usw.

### 2. Implementierung
Für die Implementierung der Algorithmen, sollte zunächst ein einfaches Graph-Modell mittels Flink 
Datastream-API erstellt werden. Die Algorithmen sollten anschließend auf Basis des Modells umgesetzt werden.

### 3. Evaluation
Da Flink ein Framework für die Verteilte-/Skalierbare- Implementierung von Streams ermöglicht, sollten alle 
umgesetzten Algorithmen im Cluster evaluiert werden. Dabei sind Parameter wie Speedup, Skalierbarkeit auf 
Datenvolumen und Skalierbarkeit auf Clustergröße von Interesse.

### 4. Vortrag
Ausarbeitung einer 10-minütigen Präsentation, welche sowohl eine kurze Vorstellung der ausgewählten 
Algorithmen beinhaltet als auch eine Diskussion über die Ergebnisse der Evaluation.

## Literatur

- [Survey Graph-Stream-Algorithms](https://people.cs.umass.edu/~mcgregor/papers/13-graphsurvey.pdf)

## Links

- [Apache Flink](https://flink.apache.org/)
- [Gelly-Stream](https://github.com/vasia/gelly-streaming)

# Free Spoken Digits

## Motivation

Ein Teilgebiet der Spacherkennung ist die Identifizierung von ausgesprochenen Ziffern, um z.B. einen Anrufender ohne menschliches Eingreifen mit dem richtigen Ansprechpartner zu verbinden.  

## Zielstellung

Der zu verwendende Datensatz, beinhaltet Audioaufnahmen von ausgesprochenen Ziffern (0-9) in Englisch, die von vier menschen realisiert wurden [FSDD](https://github.com/Jakobovski/free-spoken-digit-dataset). Für diesen Datensatz muss eine Lösung für die automatische Erkennung von Ziffern entwickelt werden.   

## Arbeitspakete

### 1. Lösungsskizze
Zunächst muss eine Literaturrecherche durchgeführt werden, um einen Überblick über die verschiedenen ML-Methoden zur Erkennung und Klassifizierung von Sprachen (Ziffern) zu verschaffen. Anhand des Ausgewählten Verfahrens und der verwendenen Daten soll eine Lösung entworfen werden.

### 2. Implementierung
Zunächst müssen die Daten in drei Teile gesplittet werden: Trainings-, Validations- und Testdaten. 
Danach kann die skizzierte Lösung mit den entsprechenden Daten trainiert, validiert und abschließend getest werden.   

### 3. Evaluierung
 Die Üblichen Metriken wie Precision und Recall könnten für die Evaluierung verwendet werden. Das Model kann eventuelle mit eigenen Sprachaufnahmen von Ziffern gestet werden.   

### 4. Vortrag
Ausarbeit einer 10-minütigen Präsentation, in welcher die Aufgabenstellung, die Lösungsskizze (kurze Beschreibung des Algorithmus/Architektur, Anwendung, sowie Nennung der verwendeten Bibliotheken) und die Ergebnisse der Evaluierung dargestellt werden. 


## Literatur

- [Raw Waveform-based Audio Classification UsingSample-level CNN Architectures](https://arxiv.org/pdf/1712.00866.pdf)
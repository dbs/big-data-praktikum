# Zu erwartende Tore (xG) im Handball 

![Graph Integration](../images/10_positionsdaten.png)

## Motivation
In den letzten Jahren ist der Bereich der Sports Analytics stark gewachsen. Sportarten wie Baseball,
Basketball, Fußball und Eishockey sind Vorreiter und beschäftigen sich ausführlich mit diesem
Thema. Im Handball ist der Bereich Sports Analytics noch nicht sonderlich ausgeprägt. Aber auch
im Handball gibt es erste Schritte zur umfassenden Datenanalyse, die zu Wettbewerbsvorteilen führen können.
Ein Wert im Fußball ist "zu erwartende Tore" (Expected Goals, xG).
Es handelt sich um einen Prognosewert zwischen 0 und 1, der angibt, ob ein Schuss oder ein Wurf zu einem Tor führt. Ein Elfmeter im Fußball hat einen xG von 0,77. Der Wert quantifiziert die Torchance. Im Fußball sind u.a. Abstand und Winkel zum Tor wichtig zur Berechnung des xG-Wertes.
Für die vorliegende Aufgabe stehen Spieldaten aus dem Handball zur Verfügung. 
Es handelt sich um zwei Datensätze:

* Tabellarische Daten (kategorische und numerische Daten,  z.B. Distanz zum Tor, Spieler ID, Spiel ID, etc.)
* Zeitreihen mit Positionen von Ball und Spieler, Beschleunigung, Geschwindigkeit, ... 


## Zielstellung
In Vorarbeiten wurden verschiedene Strategien zur Prognose des xG-Wertes untersucht (Regressionsmodelle). 
Diese Strategien sind kritisch zu prüfen. Die am erfolgversprechendsten Strategien bzw. Methoden sind auszuwählen (mit Begründung).
Es stehen zusätzliche Daten aus Spielen der Handballbundesliga aus den letzten Jahren zur Verfügung. 
Die erfolgversprechendsten Strategien sind auf die neuen umfangreichen Daten anzuwenden. 
Das Modell für den xG-Wert ist neu zu trainieren und zu evaluieren. 



## Arbeitspakete

### 1. Vorarbeiten lesen und verstehen (Daten und Methoden), angegebene Literatur lesen und verstehen

### 2. Vorarbeiten kritisch bewerten (Stärken, Schwächen, Chancen und Risiken aufzeigen)

### 3. Auswahl des oder der erfolgversprechendsten Strategien bzw. Methoden aus den abgeschlossenen Arbeiten

### 4. Den Stand der Forschung prüfen, falls in der Literatur neuere Ansätze vorgeschlagen werden, diese den Methoden bei Punkt drittens  gegenüberstellen

### 5. Neue zusätzliche Daten werden ausgehändigt, das Prognosemodell für den xG-Wert ist mit dem erweiterten Datenmaterial und den bei Punkt drittens ausgewählten Methoden neu zu trainieren

### 6. Die Ergebnisse sind zu evaluieren und mit den vorhandenen Ergebnissen zu vergleichen

### 7. Kritische Bewertung, Zusammenfassung und Ausblick

## Links (Auswahl)

* [Bundesliga Fußball, xG](https://www.bundesliga.com/de/bundesliga/news/expected-goals-xgoals-fussball-analyse-statistik-3760)
* [kinexon](https://kinexon.com/industries/kinexon-sports)
* [Sportanalyse](https://www.taylorfrancis.com/books/edit/10.1201/9781315166070/handbook-statistical-methods-analyses-sports-jim-albert-mark-glickman-tim-swartz-ruud-koning)
* [Bewertung Leistung im Handball](https://www.scitepress.org/Link.aspx?doi=10.5220/0007920001960202)


# Web application for Dynamic Visualization and Predictive Analysis of Seismic Activity in shape of graph and time-series




## Motivation

The project is inspired by the need for enhanced tools in seismology that can visualize and analyze seismic data in more sophisticated and informative ways. The complexity of seismic activities, combined with the vast network of sensors that monitor them, generates rich time-series data. The challenge lies in effectively interpreting this data to predict and understand seismic events better. Enhanced visualization tools that can map this data onto a graph, indicating the stability of sensor readings and allowing for interactive exploration, are crucial for advancing seismic research and disaster response strategies.

## Goal

To develop a web-based visualization and analysis tool that transforms seismic data into an interactive graph and time-series representation. This tool will:
Map sensor data to a graph, following the same approach described in this paper : "Graph neural networks for multivariate time series regression with application to seismic data" as a first method , and based on the similarity of time-series data between stations following the same approach as this paper : "FeatTS: Feature-based Time Series Clustering" as a second method.
Developing an algorithmic function that calculates the similarity between time-series data of different stations, adjusting the graph connections based on a predefined similarity threshold.
Highlight vertices (sensors) with unstable time-series data in red and stable ones in green.
Offer filters for users to view data within specific time periods or regions.
Dynamically updates the graph structure as new stream data is ingested, adding or removing edges based on time-series similarity measures.
Implementing an alert system that flags stations with a high degree of connectivity to unstable nodes (marked in red), turning such nodes orange as a prediction of potential risk or impact.
Offering dual visualization models: one leveraging geological information (as outlined in the referenced paper) and another visualization based on time-series data similarity.


## Work packages

### 1. Conceptualization
Architecture: The application will be designed as a modular system, capable of ingesting and processing seismic data in real-time, and presenting the information through an interactive user interface.
Class Diagrams: Essential software components will be identified
Software Selection: Choices will include data processing libraries and web development frameworks (e.g., Spring-boot + Angular/React)
API Documentation: Detailed documentation will be provided for APIs facilitating data retrieval, data mapping, real-time updates, and user interactions using OpenAPI.
### 2. Implementation

Implementing the frontend visualizations that allow users to intuitively explore seismic data and the backend infrastructure that supports data processing and analysis.
Key features will include interactive time-series charts, network graphs of the sensor layout, and tools for users to customize their data views.
### 3. Presentation
A 10-minute presentation is prepared, in which the conception and the the web application are presented.
## Dataset
Use the same dataset described in the paper :  https://github.com/smousavi05/STEAD, this dataset should first be transformed into time-series data for 3 features (acceleration, velocity and displacement) as described in github 
## Papers
https://dl.acm.org/doi/pdf/10.1145/3448016.3452757 ,  https://link.springer.com/article/10.1007/s41060-022-00349-6

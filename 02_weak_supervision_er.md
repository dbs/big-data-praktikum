# Weak Supervision für Record Linkage

## Motivation
Ein wichtiger Bestandteil von Datenintegrationprozessen mit mehreren Datenquellen ist die Erkennung von Datensätzen, 
die zur gleichen Real-Welt-Entität, z.B. einer Person, gehören. Bei diesem Record Linkage werden Datensätze i.d.R. paarweise 
auf Basis ihrer Attribute wie Name, Geburtsdatum und Adresse verglichen und als Match bzw. Non-Match klassifiziert. 
Im Allgemeinen werden für die Klassifikation Ähnlichkeiten zwischen Attributen berechnet. Die Ähnlichkeiten werden als Indikatioren 
verwendet. Hierfür wird ein Klassifikationsmodell basierend auf Trainingsdaten erstellt. Die ERstellung von Trainingsdaten ist
jedoch ein aufwändiger Prozess. Deshalb ist es wünschenwert den Labeling-Prozess zu reduizeren. Eine Idee zur Reduktion ist Weak-
Supervision, wo eine Menge von Labeling-Funktinoen definiert wird, die in der Regel keine Trainingsdaten benötigen. Basierend
auf dem erstellten Modell mittels der Labeling-Funktionen wird dann die finale Klassifikation berechnet. 

## Zielstellung  
Anhand einer bereits existierenden Library, soll eine Methode realisiert werden, 
die ein beliebiges Linkage-Problem mittels Weak Supervision löst.
Hierfür sollen als Eingabe Ähnlichkeitsvektoren zur Verfügung stehen. Für Weak Supervision ist eine Menge von Labeling-Funktionen nötig, die ebenfalls vorab definiert werden müssen.
## Aufgaben

### 1. Theoretische Grundlagen und Lösungsskizze: 

Im ersten Schritt sollen die theoretischen Grundlagen von Weak Supervision erarbeitet und verstanden 
werden. Weiterhin sollen Implementierungsmöglichkeiten mithilfe von 
vorhanden Python Bibliotheken recherchiert werden (siehe Quellen).  

### 2. Implementierung: 

Mithilfe der gewählten Bibliothek soll eine Methode realisiert werden, welches ein Modell erstellt für eine Menge von Ähnlichkeitsvektoren, Eingabedatensätze und selbstdefinierten Labelingfunktionen. 

Evaluierung und Visualisierung: 
Die implementierten Funktionen sind auf den Datensätzen zu evaluieren. 

### 3. Vortrag: 
Ausarbeitung einer 10-minütigen Präsentation, in welcher die Aufgabenstellung, die Lösungsskizze 
(Beschreibung der Verfahren, Anwendung, sowie Nennung der verwendeten Bibliotheken) und die 
Ergebnisse der Evaluierung dargestellt werden. 


## Quellen
[1] https://github.com/knodle/knodle

[2] https://arxiv.org/abs/2402.01922

[3] https://github.com/Hhhhhhao/General-Framework-Weak-Supervision

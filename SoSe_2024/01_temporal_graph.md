# Finanz-Transaktionen als Temporaler Graph

![Graph Integration](../images/01_finbench.png)

## Motivation
Graphen eignen sich um Beziehungen zwischen Entitäten zu modellieren. Entitäten, im Graph Kontext als 
Knoten (engl. Vertices) bezeichnet, können beispielsweise 
Bank-Accounts, Personen oder Firmen sein, wobei Beziehungen, im Graph-Kontext als Kanten 
(engl. Edges) bezeichnet, die Interaktion dieser Entitäten 
darstellen, z.B. Transaktionen zwischen Bank-Accounts.
Betrachtet man die Entwicklung eines solchen Graphen über die Zeit, spricht man von einem Temporalen 
Graphen. Dieser beinhaltet die komplette Historie des Graphen, also konkret _wann_ Entitäten und 
deren Beziehungen entstanden sind und ggf. _wie lange_ diese gültig sind.
Die zusätzliche Zeitdimension ermöglicht deutlich vielfältigere Analysen, als die rein statische 
Betrachtung des Netzwerks zu einem Zeitpunkt. 

Das Linked Data Benchmark Council (LDBC) hat 2023 einen neuen Graph-Datenbank Benchmark namens "FinBEnch" entwickelt. Neben einer Evaluations-Pipeline existiert auch ein Datengenerator sowie eine Liste von Einfachen- und Komplexen Graph-Queries, die Bestandteil des Benchmarks sind. Das Ziel von FinBench ist es, ein einheitlichen Benchmark für Graph-Datenbanken anzubieten, um diese Vergleichbar zu machen.

An der Abteilung Datenbanken ist duch eine ehemalige Masterarbeit eine (Bi)-Temporale Graph-Datenbank entstanden, welche auf der relationalen Datenbank MariaDB aufsetzt. Das System ist in Java geschrieben und besitzt grundlegenden Funktionalitäten, wie CRUD Anweisungen für Knoten und Kanten. Auch ein Temporales Pattern-Matching unter Nutzung der Abfragesprache GDL wurde umgesetzt.

## Zielstellung
Ziel des Praktikums ist es, (1) den Finbench-Datensatz in die (Bi)-Temporale Graph DB zu importieren, (2) die Datenbank in den vom LDBC vorgegebenen Benchmark-Workflow einzubinden und (3) die knapp 20 Queries umzusetzen und damit die Graph-DB zu benchmarken.

### (1) Importer
Beim Import sollen die CSV-Dateien des Finbench-Datensatzes eingelesen werden. Es kann sich dabei an dem vom LDBC vorgegebenen Graph-Schema orientiert werden (siehe Bild oben). Ist der Graph importiert, kann mit einfachen Lesebefehlen die Funktionsweise getestet werden, z.B. gib mir alle Knoten vom Typ "Account". 

### (2) Einbindung in den Benchmark
FinBEnch stellt einen Benchmark-Driver zur Evaluation einer Graph-DB bereit. Ziel ist es, die hier betrachtete Temporale Graph DB in diesen Prozess einzubinden. Eine Besonderheit stellen bspw. die Parameter für die gegeben Queries dar. Für einen erzeugten Datensatz, auf den der Benchmark ausgeführt werden soll, und die Einfachen- und Komplexen Graph-Queries, stehen verschiedene Query-Parameter bereit, die dann im Benchmark verwendet werden sollen.

### (3) Umsetzung der Queries
Im dritten Teil sollen möglichst viele der Simple/Complex Read Queries umgesetzt werden, welche in der FinBench-Dokumentation ausführlich definiert wurden. Zur Umsetzung soll das  temporale Pattern-Matching der Graph-DB verwendet werden.

## Arbeitspakete

### 1. Einarbeitung und Lösungsskizze
Zunächst sollten Sie sich ausführlich mit der Graph-Datenbank und deren Funktionsweise auseinandersetzen. Dann soll in einem Konzept skizziert werden, wie die Datensätze importiert werden. Dann muss sich mit dem BEnchmark auseinander gesetzt werden und der Funktionsweise des Treibers sowie die Experimente zu den Queries. In der Lösungsskizze sollen zudem die entsprechenden Queries formuliert und angegeben werden. Diese können ggf. dann im zweiten Teil noch fenjustiert werden.

### 2. Implementierung und Evaluation
Basierend auf der Lösungsskizze ist der experimentelle Aufbau mit der Graph-DB umzusetzen. Es sollen drei Implementierungen entstehen:
1. Daten-Importer: Lesen der Datensätze + Erstellen des Graphen in der DB
2. Graph-Queries: Die Entwicklung der zu den Benchmark-Queries zugehörigen GDL-Queries
3. Benchmark-Umsetzung: Die Einbindung der Graph-DB in den FinBench Benchmark

### 3. Vortrag
Ausarbeitung einer 10-minütigen Präsentation, in welcher die Aufgabenstellung, die Lösungsskizze und die Ergebnisse dargestellt werden.

## Links

* [FinBench Dokumentation](https://arxiv.org/pdf/2306.15975.pdf)
* [FinBench Overview Slideset](https://ldbcouncil.org/benchmarks/finbench/finbench-talk-16th-tuc.pdf)
* [Masterarbeit](https://dbs.uni-leipzig.de/files/study/theses/2021/pdf/Masterarbeit_Fritzsche_2021.pdf)
* [VLDB Journal article Gradoop + Temporal GDL](https://link.springer.com/article/10.1007/s00778-021-00667-4)
* [Temporal GDL - Github](https://github.com/dbs-leipzig/gdl)

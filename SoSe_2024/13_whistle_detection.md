# Whistle-Detection im Roboterfußball

![Game](../images/13_game.jpg)

## Motivation
Ziel des [RoboCups](https://www.robocup.org/) ist es, bis zur Mitte des 21. Jahrhunderts den menschlichen Fußballweltmeister mit Robotern nach Regeln der FIFA zu schlagen. Dafür gibt es regelmäßige Verschärfungen der Spielregeln. Seit mehreren Jahren müssen die Roboter zu Beginn des Spiels, während die Roboter sich noch im festen Stand befinden, die Pfeife erkennen. Neu seit einem Jahr ist, dass auch während des Spiel Pfiffe, z. B. bei einem Tor, registriert werden müssen. Die Geräuschkulisse während des Spiel ist erheblich Lauter, vor allem, da der Roboter seine eigene Laufbewegung hört.

In der [SPL](https://spl.robocup.org/) wird standardmäßig aus Teams mit dem [NAO](https://de.wikipedia.org/wiki/Nao_(Roboter)) gespielt. Dieser verfügt jedoch nur über eine begrenzte Rechenleistung, weshalb eine Whistle-Detection performant funktionieren muss.


## Zielstellung
Ziel ist die Entwicklung einer Whistle-Detektion mit folgenden Prioritäten:
1. Minimierung von Fehlerkennungen (Falsch-positive -> schlecht)
2. Erkennung von Pfiff-Events ("überhören" eines Pfiffs nicht so schlimm wie Falsch-positive)
3. Minimierung der Laufzeit

weitere Kriterien (optional):
- Unterscheidung von verschiedenen Pfeifen (wurde mit der gleichen Pfeife/vom gleichen Person gepfiffen)
- Distanz-/Richtungsschätzung zum detektierten Pfiff
- Portierung zur Zielarchitektur (C++)
- Widerstandsfähigkeit zu "buntem" Rauschen

## Arbeitspakete
1. vertraut machen mit den Daten (Tonaufnahmen der Roboter aus Spielen und im Labor, Daten werden zur Verfügung gestellt)
2. Recherche zu Audioverarbeitung und Sound-Eventerkennung
3. Implementierung der Sounderkenung
4. Evaluierung
5. Dokumentation der Methoden und Ergebnisse
6. Vortrag

## Links (Auswahl)
* [Fourier-Transformation](https://www.youtube.com/watch?v=spUNpyF58BY)
* [Oberton](https://de.wikipedia.org/wiki/Oberton)
* [Neural Network and Prior Knowledge Ensemble for Whistle Recognition (S. 17(28))](https://link.springer.com/chapter/10.1007/978-3-031-55015-7_2)
* [Single- and Multi-Channel Whistle Recognition
with NAO Robots](https://citeseerx.ist.psu.edu/document?repid=rep1&type=pdf&doi=8695910dd826d5174db88258a302128ad3cb824f)
* [Deep Learning for Audio Signal Processing](https://ieeexplore.ieee.org/stamp/stamp.jsp?arnumber=8678825&casa_token=EYFFNgCO2Y0AAAAA:wWnWuAJG74nFl98jUp4L9Ahr1T5ki7WZiB2AjKLJiRTACk_PJ4LpdiZxnFejdODAM2aMfdp0&tag=1)
* [LEAF](https://arxiv.org/pdf/2101.08596.pdf)

![Whistle_Spectrum](../images/13_whistle.png)

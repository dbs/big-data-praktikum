# Does fake data make you private?

## Description

In this project, we aim to examine datasets containing demographic data, some of which may be falsified by the survey respondents. Users might alter details such as age, gender, ethnicity, weight, and income for privacy reasons or personal satisfaction. However, it’s uncertain whether these alterations enhance their privacy or make them more distinctive based on the remaining data. For instance, providing false gender data doesn’t necessarily improve your privacy if no other person shares your age. This issue arises when users are solely responsible for their privacy without additional information about our data collection. Our initial hypothesis is that ‘faking it doesn’t make you private,’ and we plan to test this by evaluating simulated scenarios. We’ll use a public dataset with the required attributes and apply an obfuscation or ‘faking’ method based on various distributions of real and fake users/attributes. We’ll then assess how the dataset evolves and specifically analyze each user’s uniqueness and privacy in each dataset version. Ultimately, we’ll compare these results for the original and various fake data distributions to draw conclusions about our hypothesis. Other datasets can also be used to validate the results. A key question is whether offering users the simple option to provide fake or generalized data actually enhances or potentially compromises their privacy.

### 1. Data Generator
In this work package, we will create a general data generator for the project. This involves selecting relevant demographic attributes from public datasets and applying an obfuscation or ‘faking’ method based on various distributions of real and fake users/attributes. The method will be used for a selected dataset but should be general enough to work on others that have the same attributes. The goal is to generate datasets that can simulate different levels of fake data, allowing for the analysis of each user’s uniqueness and privacy in each version of the dataset.

### 2. Privacy Analysis
In the Privacy Analysis work package, we will analyze the generated datasets. We will compare the original and various fake data distributions to assess the impact of the ‘faking’ method on user privacy. The aim is to draw conclusions about the initial hypothesis: ‘faking it doesn’t make you private.’ We will also explore whether providing fake or generalized data actually enhances or potentially compromises user privacy. This will involve the use of statistical analysis and possibly machine learning techniques to identify patterns and trends in the data.

### 3. Presentation
Finally, a 10-minute presentation is prepared, in which the task, the approaches to the solution, and the results are presented.

## Datasets
- Any census dataset should work well, e.g. U.S. Census or NCVR
- Any other dataset using demographic information, e.g. many health datasets (https://www.kaggle.com/datasets/alexteboul/diabetes-health-indicators-dataset or https://archive.ics.uci.edu/dataset/296/diabetes+130-us+hospitals+for+years+1999-2008)
- Might be interesting to choose datasets with different sizes and topics to see how they influence the privacy risks

## Literature
#### Some motivation:
"99.98% of Americans would be correctly re-identified in any dataset using 15 demographic attributes such as age, gender and marital status"
- https://www.nature.com/articles/s41467-019-10933-3

#### Testing disclosure privacy in practice:
This paper shows how the U.S. Census Bureau tries to generally fight against disclosure of identities in large demographic datasets and tests how well it works.
- https://link.springer.com/article/10.1007/s11113-023-09829-4

#### Related works using fake data for privacy:
- https://dl.acm.org/doi/abs/10.1145/1655188.1655204
- https://aisel.aisnet.org/bled2019/53/

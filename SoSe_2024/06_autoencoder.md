# Unüberwachtes Lernen mittels Autoencoder auf single-cell Genexpressionsdaten

**Voraussetzung**: Python-Programmierkenntnisse

## Motivation

Moderne Sequenziermethoden ermöglichen, dass die Genexpression einzelner Zellen (single-cell) gemessen werden kann und riesige Datenmengen in Datenbanken wie dem CZ CELLxGENE Discover katalogisiert sind. Einzelne Experimente umfassen oftmals Millionen von Zellen für die bis zu 30.000 Gene (Features) gemessen werden. Zur Dimensionsreduktion und biologischen Gruppierung der Zellen haben sich Autoencoder als Methode zum unüberwachten Lernen etabliert. Im Vergleich zu klassischen Methoden wie Principle Component Analysis rekonstruieren Autoencoder besser die nicht-linearen Zusammenhänge der Genexpression und somit den Zustand einer menschlichen Zelle (gesund, krank, Tumor, Immunaktivierung).

## Zielstellung

Im Projekt soll eine auf Python-basierte Lösung entwickeln werden. Im ersten Schritt soll diese einen Datensatz vom CZ CELLxGENE Discover über die API im anndata-Dateiformat herunterladen und einlesen können. Die erhaltene Genexpressionsmatrix ist die Grundlage für das Lernen eines Autoencoders, wessen Architektur eigenständig implementiert werden soll. Zu guter Letzt soll der Latent Space des Autoencoder visualisiert werden, um Gruppierungen (Cluster) der Zellen darzustellen.

## Arbeitspakete

__1 - Anwendungsdesign und Lösungsskizze__

In einer Lösungsskizze sollen alle Schritte von der Datenabfrage, Implementierung des Autoencoders und Visualisierung festgehalten werden. Weiterhin soll ein Beispieldatensatz definiert werden und eine Architektur des Autoencoders festgelegt werden.

__2 - Implementierung und Evaluation__

Basierend auf der Lösungsskizze ist die Abfrage und Einlesen von anndata-Dateien des CZ CELLxGENE Discover zu implementieren. Darauf aufbauend soll die Autoencoder Architektur z.B. mittels PyTorch implementiert und mit den Daten trainiert werden. Zuletzt sollen Visualisierungen des Latent Spaces und der Loss Curve zur technischen und biologischen Evaluierung herangezogen werden.

__3 - Vortrag__

Ausarbeitung einer 10-minütigen Präsentation, in welcher die Aufgabenstellung, die Lösungsskizze und die Evaluationsergebnisse vorgestellt werden.

## Literatur & Links

* [CZ CELLxGENE] <https://cellxgene.cziscience.com/>
* [anndata Format] <https://anndata.readthedocs.io/en/latest/>
* [Single Cell] 2min explaination <https://www.youtube.com/watch?v=6UVOdCc1Q7I> 
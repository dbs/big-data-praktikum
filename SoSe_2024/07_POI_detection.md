﻿# Optimale Embeddings zur Point of interest Erkennung in Georouten

## Motivation

Durch die stark gestiegene Nutzung von Smartphones im Alltag und dabei insbesondere von Standort-basierten Diensten wie Google Maps, Foursquare oder Strava, werden geographische Daten von Personen im großen Umfang gesammelt und verarbeitet. Solche Daten sind jedoch sehr Privatsphäre-kritisch, da sie oft so genannte Points of interest (POI) enthalten, die private Details über eine Person verraten können. Points of interest bezeichnen geographische Punkte, die mit einer semantischen Information verknüpft sind. Dies können private Aufenthaltsorte einer Person sein, wie bspw. ihr Wohn- oder Arbeitsort, oder auch öffentliche Orte, wie z.B. Restaurants, Bildungseinrichtungen oder Fitnessclubs. Zum Schutz der Daten müssen diese privatisiert werden. Um die Effizienz von Schutzmechanismen prüfen zu können, werden Evaluierungsmethoden benötigt, die das initiale und nach einer Privatisierung verbleibende Privatsphärerisiko in den Daten bemessen.
In diesem Projekt soll daher der folgende Ansatz zur Point of interest Erkennung getestet werden:
Es soll ein Machine Learning Modell auf Standortverlaufsdaten (Trajektorien) trainiert werden, um zu erkennen, ob die Trajektorie einen POI enthält oder nicht. Dazu sollen fortlaufende Standortverlaufsdaten in gleichlange Subrouten zerlegt und entsprechend klassifiziert werden. Dabei liegt der Hauptfokus der Arbeit darauf, ein geeignetes Encoding für die Geodaten zu finden, welches für den Anwendungsfall gut funktioniert.

## Zielstellung

Die Aufgabe des Projektes ist es, ein geeignetes Machine Learning Modell zu trainieren, welches Standortverlaufsdaten dahingehend klassifiziert, ob sie POIs enthalten oder nicht. Dabei sollen insbesondere verschiedene Embedding-Ansätze für Geodaten getestet werden. Hierfür ist ein Datensatz bestehend aus Taxi-Routen mit POI-Markierung verfügbar.

## Arbeitspakete

### 1. Datenvorverarbeitung
Die zur Verfügung gestellten Daten sollen in Teilstücke fester Länge aufgeteilt werden. Das Enthaltensein eines POI stellt dabei die Ground truth für den Klassifizierungstask dar. Die Daten sind in Trainings-, Validierungs- und Testdaten aufzuteilen.

### 2. Modellierung
Anschließend soll ein geeignetes Machine Learning Modell zur Lösung des Klassifizierungstasks trainiert werden. Hierbei sollen mehrere geeignete Embedding-Ansätze für Geodaten recherchiert und getestet werden. Anschließend sollen die Modelle mithilfe von Precision, Recall und Accuracy evaluiert werden. Abschließend soll für die besten Modelle die Testperformanz mit den gleichen Metriken auf den Testdaten ermittelt werden.

### 3. Vortrag
Abschließend erfolgt die Ausarbeitung einer 10-minütigen Präsentation, in welcher die Aufgabenstellung, die Lösungsskizze (Modell- und Merkmalsauswahl) und die Ergebnisse dargestellt werden. 


## Literatur & Datensätze
- Mai, G., Janowicz, K., Hu, Y., Gao, S., Yan, B., Zhu, R., Cai, L., Lao, N., 2022. A Review of Location Encoding for GeoAI: Methods and Applications. International Journal of Geographical Information Science 36, 639–673. https://arxiv.org/abs/2111.04006
- Nawaz, A., Zhiqiu, H., Senzhang, W., Hussain, Y., Khan, I., Khan, Z., 2020. Convolutional LSTM based transportation mode learning from raw GPS trajectories. IET Intelligent Transport Systems 14, 570–577. https://doi.org/10.1049/iet-its.2019.0017
- Udapudi, A.D., 2022. Geospatial Trip Data Generation Using Deep Neural Networks.

- Daten: Porto Taxi Datensatz wird in vorverarbeiteter Form zur Verfügung gestellt

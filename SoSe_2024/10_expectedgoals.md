# Zu erwartende Tore (xG) im Handball 

![Graph Integration](../images/10_positionsdaten.png)

## Motivation
In den letzten Jahren ist der Bereich der Sports Analytics stark gewachsen. Sportarten wie Baseball,
Basketball, Fußball und Eishockey sind Vorreiter und beschäftigen sich ausführlich mit diesem
Thema. Im Handball ist der Bereich Sports Analytics noch nicht sonderlich ausgeprägt. Aber auch
im Handball gibt es erste Schritte zur umfassenden Datenanalyse, die zu Wettbewerbsvorteilen führen können.
Ein Wert im Fußball ist "zu erwartende Tore" (Expected Goals, xG).
Es handelt sich um einen Prognosewert zwischen 0 und 1, der angibt, ob ein Schuss oder ein Wurf zu einem Tor führt. Ein Elfmeter im Fußball hat einen xG von 0,77. Der Wert quantifiziert die Torchance. Im Fußball sind u.a. Abstand und Winkel zum Tor wichtig zur Berechnung des xG-Wertes.
Für die vorliegende Aufgabe stehen Spieldaten aus dem Handball zur Verfügung. Es handelt sich um zwei Datensätze, zum einen um tabellarische Daten (Statistik des Spiels,  z.B. Wurfwinkel und Distanz zum Tor) und zum anderen um detaillierte Bewegungsdaten während des Spiels (Zeitreihen, u.a. Pos. des Spielers, Geschwindigkeit). 


## Zielstellung
Es ist auf der Basis der Daten ein Ansatz zur Berechnung eines xG-Wertes zu entwickeln.



## Arbeitspakete

### 1. Datensätze getrennt einlesen in (Jupyter Notebook)

### 2. Daten anzeigen und verstehen

### 3. Kombinieren der Datensätze

### 4. Verfahren zur Berechnung des xG-Wertes entwerfen (mittels Literatur)

### 5. Das Verfahren anwenden und Ergebnisse bewerten

### 6. Zusammenfassung erstellen

## Links (Auswahl)

* [Bundesliga Fußball, xG](https://www.bundesliga.com/de/bundesliga/news/expected-goals-xgoals-fussball-analyse-statistik-3760)
* [kinexon](https://kinexon.com/industries/kinexon-sports)
* [Sportanalyse](https://www.taylorfrancis.com/books/edit/10.1201/9781315166070/handbook-statistical-methods-analyses-sports-jim-albert-mark-glickman-tim-swartz-ruud-koning)
* [Bewertung Leistung im Handball](https://www.scitepress.org/Link.aspx?doi=10.5220/0007920001960202)

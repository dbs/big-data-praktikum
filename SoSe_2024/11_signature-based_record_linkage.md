# Signature-based Record Linkage
## Motivation
Ein wichtiger Bestandteil von Datenintegrationprozessen mit mehreren Datenquellen ist die Erkennung von Datensätzen, die zur gleichen Real-Welt-Entität, z.B. einer Person, gehören. Bei diesem Record Linkage werden Datensätze i.d.R. paarweise anhand ihrer Attribute wie Name, Geburtsdatum und Adresse verglichen und als Match bzw. Non-Match klassifiziert. 
Ein alternativer Ansatz zu diesem konventionellen Verfahren basiert auf Record-Signaturen mit Subrecords, siehe [Zhang2018].

## Zielstellung
Ziel des Praktikums ist es, das beschriebene Signatur-basierte Verfahren zu reimplementieren und auf einem gegebenen Datensatz zu evaluieren.

## Arbeitspakete

### 1. Einarbeitung und Lösungsskizze
Zunächst soll das Paper zum Signatur-basierten Verfahren studiert und eine Lösungsskizze für die Reimplementierung erarbeitet werden.

### 2. Implementierung und Evaluierung
Der bereitgestellte Datensatz ist in eine Datenbank zu importieren. Die beim Ausführen generierten Signaturen und finalen Entity-Zuordnungen sind ebenfalls in der Datenbank persistent zu speichern und im Anschluss anhand der  bereitgestellten Ground-Truth zu evaluieren.

### 3. Vortrag
Ausarbeitung einer 10-minütigen Präsentation, in welcher die Aufgabenstellung, die Lösungsskizze und die Ergebnisse dargestellt werden.

## Material

* [Zhang2018: Scalable Entity Resolution Using Probabilistic Signatures on Parallel Databases](https://dl.acm.org/doi/abs/10.1145/3269206.3272016)
* [Benchmark datasets](https://dbs.uni-leipzig.de/research/projects/benchmark-datasets-for-entity-resolution)